execute pathogen#infect()

filetype plugin indent on

" ===== Plugin Settings =====

call neomake#configure#automake('w')

let g:NERDTreeDirArrowExpandable = 'x'
let g:NERDTreeDirArrowCollapsible = '-'
let g:NERDTreeIgnore = ['\.pyc$', '^__pycache__$']
let g:NERDTreeMinimalUI = 1
let g:NERDTreeMouseMode = 2
let g:NERDTreeSortOrder = []
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
let g:ctrlp_use_caching = 0
let g:elm_setup_keybindings = 0
let g:neomake_logfile = '/tmp/neomake.log'
let g:neomake_elixir_enabled_makers = ['mix', 'credo']
let g:seoul256_background = 235

set wildignore+=*.beam,*/node_modules/*,*/platforms/*,*/vendor/bundle/*,*__pycache__

" ===== Color/UI Settings =====

syntax on
colorscheme seoul256

set background=dark
set fillchars+=vert:\ "

hi VertSplit ctermbg=1

" ===== Status-line Settings =====

set laststatus=2

set statusline=%f\ %m%=%y\ %l:%c

" ===== General Settings =====

set backspace=indent,eol,start
set nonumber
set scrolloff=5
set autoread
set showmode
set nowrap
set hidden
set mouse=a
set clipboard=unnamed,unnamedplus
"set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<

" ===== Performance Settings =====

set nocursorline
set norelativenumber
set regexpengine=1

" ===== Folding Settings =====

set foldmethod=indent
set nofoldenable

" ===== Search Settings =====

set incsearch
set hlsearch

" ===== Swap/backup Settings =====

set noswapfile
set nobackup
set nowb

" ===== Indentation Settings =====

set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

set listchars=tab:>-,space:.,trail:~
set nolist

" ===== Mappings =====

let mapleader=","

nnoremap <leader>ev :e $MYVIMRC<cr>

nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gd :Gdiff<cr>

nnoremap <leader>c :close<cr>
nnoremap <leader>h :noh<cr>
nnoremap <leader>p :set invpaste paste?<cr>
nnoremap <leader>w :write<cr>

nnoremap <leader>s :split<cr><C-w><C-w>
nnoremap <leader>v :vsplit<cr><C-w><C-w>

nnoremap <leader>f :Neoformat<cr>
nnoremap <leader>m :Neomake<cr>
nnoremap <leader>n :NERDTreeToggle<cr>
nnoremap <leader>t :CtrlPClearCache<cr>:CtrlP:<cr>

" Show name of highlight group
nnoremap <leader>y :echo synIDattr(synID(line("."),col("."),0),"name")<cr>

inoremap <C-c> <esc>
tnoremap <C-\> <C-\><C-n>

nnoremap <A-,> :tabprev<cr>
nnoremap <A-.> :tabnext<cr>
tnoremap <A-,> <C-\><C-n>:tabprev<cr>
tnoremap <A-.> <C-\><C-n>:tabnext<cr>

nnoremap <leader>t :CtrlP<cr>
nnoremap <leader>b :CtrlPBuffer<cr>

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l

nnoremap H ^
nnoremap L $
onoremap H ^
onoremap L $
vnoremap H ^
vnoremap L $

" ===== Autocommands =====

autocmd BufWritePre * :%s/\s\+$//e " Kill trailing spaces on save

augroup filetypes
  autocmd BufNewFile,BufRead *.adoc setf asciidoc
  autocmd BufNewFile,BufRead *.json setf javascript
augroup END

augroup neoformat
  autocmd! BufWritePre * Neoformat
augroup END

augroup neomake
  autocmd! BufWritePost * Neomake
augroup END
